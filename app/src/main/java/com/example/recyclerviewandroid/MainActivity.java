package com.example.recyclerviewandroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ArrayList<User>usersList;
    private RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView=findViewById(R.id.recyclerView);
        usersList=new ArrayList<>();
        setUserInfo();
        setAdapter();
    }

    private void setAdapter() {
        recyclerAdapter adapter= new recyclerAdapter(usersList);
        RecyclerView.LayoutManager layoutManager= new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    private void setUserInfo() {
        usersList.add(new User("Mark Selby"));
        usersList.add(new User("Judd Trump"));
        usersList.add(new User("Ronnie O'Sullivan"));
        usersList.add(new User("Neil Robertson"));
        usersList.add(new User("Kyren Wilson"));
        usersList.add(new User("John Higgins"));
        usersList.add(new User("Shaun Murphy"));
        usersList.add(new User("Mark Williams"));
        usersList.add(new User("Zhao Xintong"));
        usersList.add(new User("Barry Hawkins"));
        usersList.add(new User("Stephen Maguire"));
        usersList.add(new User("Stuart Bingham"));
        usersList.add(new User("Mark Allen"));
        usersList.add(new User("Anthony McGill"));
        usersList.add(new User("Yan Bingtao"));
        usersList.add(new User("Luca Brecel"));
    }
}